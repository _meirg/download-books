#!/bin/bash 
#author: רייסטלין

BOOKS_DATA_URL=https://openbooks.openu.ac.il/booksdata
BOOKS=$(curl -s $BOOKS_DATA_URL | jq '.books')

logger() {
    status=$?
    if [[ $status -ne 0 ]] ; then 
        echo -e "\e[91m[!] $@"
    else
        echo -e "\e[92m[*] $@"
    fi
}

download(){
    tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
    q='.[$index] | [.oliveId, .name, .subTitle, .departments[].name] | @tsv'
    jq -r --argjson index $1 "$q" <<< "$BOOKS" | while IFS=$'\t' read -r book_is book_name subTitle departments;
    do 
        logger "Start Download: $book_is $book_name $(echo $subTitle | sed 's/[;//]/,/g') $departments"
        url="https://olvreader.sefereshet.org.il/Olive/OTB/OpenU/GetImage.ashx?kind=page&href=$book_is&page="
        for page in {1..2000}; 
        do 
            name=$(printf %03d.jpg $page)
            wget -q "${url}${page}" -O "${tmp_dir}/${name}" || break; 
        done
        dst="$HOME/Library/OpenUniversity/${departments}/${book_name}"
        mkdir -p "$dst"
        rm -f "${tmp_dir}/${name}";
        convert "${tmp_dir}/*" "${dst}/${subTitle:-$book_name}.pdf" 2> /dev/null
        rm -rf "$tmp_dir"
    done
}

download_books(){
    END=$(jq length <<< "$BOOKS")
    for (( i=${1:-1}; i<=END; i++ ));
    do  
        download $i
    done
}

download_category_books(){
    q='. | map(.departments[].name==$category) | indices(true) | .[]'
    books_ids=$(jq -r --arg category "$1" "$q" <<< "$BOOKS")
    logger $1
    for book_id in $books_ids; 
    do
        download $book_id
    done
}

get_books_from_category(){
    categories=([1]="המחלקה להיסטוריה, פילוסופיה ומדעי היהדות"
    [2]="המחלקה לחינוך ולפסיכולוגיה"
    [3]="המחלקה למדעי הטבע והחיים"
    [4]="המחלקה למתמטיקה ולמדעי המחשב"
    [5]="המחלקה לניהול ולכלכלה"
    [6]="המחלקה לסוציולוגיה, למדע המדינה ולתקשורת"
    [7]="המחלקה לספרות, ללשון ולאמנויות"
    [0]="הורד את כל התוכן הזמין")
    logger "Selected: ${categories[$1]}"
    if [[ $1 -eq 0 ]]; then
        download_books
    else
        download_category_books "${categories[$1]}"
    fi
}

get_categories(){
    categories=$(jq -r '.[].departments[].name' <<< "$BOOKS" | sort -u)
    printf "%30s \n" "$categories" | nl
}

# =============
# Categoty Menu 
# =============

read -p "Welcome to library
which books do you want to take today?
press 0 to download all the books
or select department to download

$(get_categories)

Select Option: " category
get_books_from_category $category


